<!-- 
 * This file is part of the calendar module for Zwii.
 * For full copyright and license information, please see the LICENSE
 * file that was distributed with this source code.
 *
 * @author Adrien Fallot <https://gitlab.com/adrien.fallot>
 * @license GNU General Public License, version 3
-->

<link rel="stylesheet" href="./module/calendar/view/config/config.css">

<?php echo template::formOpen('calendarConfig'); ?>
	<div class="row">
		<div class="col2">
			<?php echo template::button('calendarConfigBack', [
				'class' => 'buttonGrey',
				'href' => helper::baseUrl() . 'page/edit/' . $this->getUrl(0),
				'ico' => 'left',
				'value' => 'Retour'
			]); ?>
		</div>
		<div class="col2 offset8">
			<?php echo template::submit('calendarConfigSubmit'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col7">
			<div class="block">
				<h4>Calendrier</h4>
				<?php echo template::text('calendarConfigUrl', [
					'label' => 'Lien du fichier iCal',
					'placeholder' => 'http://',
					'value' => $this->getData(['module', $this->getUrl(0), 'url']),
					'help' => 'Dois être un lien iCal valide et accessible sans authentification (en lecture seule à minimum)'
				]); ?>
			</div>
		</div>
        <div class="col5">
			<div class="block">
				<h4>Affichage</h4>
                <div class="row col12">
                    <?php echo template::select('calendarConfigLang', $module::$moduleLangage, [
                        'label' => 'Langue d\'affichage',
                        'selected' => $this->getData(['module', $this->getUrl(0), 'displayLangage'])
                    ]); ?>
                </div>
                <div class="row">
                    <div class="col6">
                        <?php echo template::checkbox('calendarConfigDisplayCalendar', true, 'Calendrier PC', [
                            'checked' => $this->getData(['module', $this->getUrl(0), 'displayCalendar'])
                        ]); ?>
                        <?php echo template::checkbox('calendarConfigDisplayMobileCalendar', true, 'Calendrier mobile', [
                            'checked' => $this->getData(['module', $this->getUrl(0), 'displayMobileCalendar'])
                        ]); ?>
                    </div>
                    <div class="col6">
                        <?php echo template::checkbox('calendarConfigDisplayDetails', true, 'Détails', [
                            'checked' => $this->getData(['module', $this->getUrl(0), 'displayDetails'])
                        ]); ?>
                        <?php echo template::checkbox('calendarConfigDisplayURL', true, 'iCal URL', [
                            'checked' => $this->getData(['module', $this->getUrl(0), 'displayURL'])
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col12">
			<div class="block">
				<h4>Plage horaire</h4>
                <div class="row">
                    <div class="col4">
                        <div class="row">
                            <div class="col6">
                                <?php echo template::text('calendarConfigStartDate', [
                                    'placeholder' => '03/04/2023',
                                    'label' => 'Date de début',
                                    'value' => $this->getData(['module', $this->getUrl(0), 'startDate']),
                                    'help' => 'Format JJ/MM/YYYY'
                                ]); ?>
                            </div>
                            <div class="col6">
                                <?php echo template::text('calendarConfigStartHour', [
                                    'placeholder' => '7:00',
                                    'label' => 'Heure de début',
                                    'value' => $this->getData(['module', $this->getUrl(0), 'startHour']),
                                    'help' => 'Format HH:MM'
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col4">
                        <div class="row">
                            <div class="col6">
                                <?php echo template::text('calendarConfigEndDate', [
                                    'placeholder' => '07/04/2023',
                                    'label' => 'Date de fin',
                                    'value' => $this->getData(['module', $this->getUrl(0), 'endDate']),
                                    'help' => 'Format JJ/MM/YYYY'
                                ]); ?>
                            </div>
                            <div class="col6">
                                <?php echo template::text('calendarConfigEndHour', [
                                    'placeholder' => '19:00',
                                    'label' => 'Heure de fin',
                                    'value' => $this->getData(['module', $this->getUrl(0), 'endHour']),
                                    'help' => 'Format HH:MM'
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col4">
                        <?php echo template::text('calendarConfigTimeZone', [
                            'placeholder' => 'Europe/Paris',
                            'label' => 'Fuseau horaires',
                            'value' => $this->getData(['module', $this->getUrl(0), 'timeZone']),
                            'help' => 'Format region/zone, voir <a href=\'https://www.php.net/manual/en/timezones.php\'>List of Supported Timezones</a>'
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="col12">
            <div class="block">
                <h4>Apparence</h4>
                <div class="row">
                    <div class="col3">
                        <?php echo template::text('calendarConfigCellHeight', [
                            'placeholder' => '30px',
                            'label' => 'Hauteur des cellules',
                            'value' => $this->getData(['module', $this->getUrl(0), 'cellHeight']),
                            'help' => 'en px'
                        ]); ?>
                    </div>
                    <div class="col3">
                        <?php echo template::text('calendarConfigDefaultBackgroundColor', [
                            'class' => 'colorPicker',
                            'help' => 'Le curseur horizontal règle le niveau de transparence.',
                            'label' => 'Couleur par défaut du fond',
                            'value' => $this->getData(['module',  $this->getUrl(0), 'eventDefaultBackgroundColor'])
                        ]); ?>
                    </div>
                    <div class="col3">
                        <?php echo template::text('calendarConfigDefaultTextColor', [
                            'class' => 'colorPicker',
                            'help' => 'Le curseur horizontal règle le niveau de transparence.',
                            'label' => 'Couleur par défaut du texte',
                            'value' => $this->getData(['module',  $this->getUrl(0), 'eventDefaultTextColor'])
                        ]); ?>
                    </div>
                    <div class="col3">
                        <?php echo template::text('calendarConfigDefaultBordersColor', [
                            'class' => 'colorPicker',
                            'help' => 'Le curseur horizontal règle le niveau de transparence.',
                            'label' => 'Couleur par défaut des bordures',
                            'value' => $this->getData(['module',  $this->getUrl(0), 'eventDefaultBordersColor'])
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col12">
            <div class="block">
                <h4>Catégories</h4>
                <div class="row">
                    <?php 
                        if($module::$categories)
                        {
                            echo template::table([3, 3, 3, 3], $module::$categories, ['Nom'.template::help("Doit être présent dans le fichier iCal"), 'Couleur du fond'.template::help("Le curseur horizontal règle le niveau de transparence."), 'Couleur du texte'.template::help("Le curseur horizontal règle le niveau de transparence."), 'Couleur des bordures'.template::help("Le curseur horizontal règle le niveau de transparence.")]);
                        }
                        else
                        {
                            echo template::speech('Aucune catégories.');
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php echo template::formClose(); ?>
<div class="moduleVersion">
	<?php echo $module::REALNAME." Version n°".$module::VERSION; ?>
</div>