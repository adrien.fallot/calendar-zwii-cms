<!-- 
 * This file is part of the calendar module for Zwii.
 * For full copyright and license information, please see the LICENSE
 * file that was distributed with this source code.
 *
 * @author Adrien Fallot <https://gitlab.com/adrien.fallot>
 * @license GNU General Public License, version 3
-->

function openDetails(target) {

	$href = $(target);

	$("body, html").animate({
		scrollTop: $($href).parent().parent().offset().top - 100
		}, 400).promise().done(function(event) {
				$($href).trigger( "click" );
		});
};

function goToDay(target) {

	$href = $(target);

	$("body, html").animate({
		scrollTop: $($href).offset().top - 100
		}, 400);
};

