<!-- 
 * This file is part of the calendar module for Zwii.
 * For full copyright and license information, please see the LICENSE
 * file that was distributed with this source code.
 *
 * @author Adrien Fallot <https://gitlab.com/adrien.fallot>
 * @license GNU General Public License, version 3
-->

<link rel="stylesheet" href="./module/calendar/view/index/index.css">

<?php 

	use ICal\ICal;
	use MobileDetect\MobileDetect;
	
	$ical = null;
	$detect = new \Detection\MobileDetect;

	// Pour la première installation, initialisation des données du module à des valeurs par défaut
	$calendarURL = $this->getData(['module', $this->getUrl(0), 'url']);
	if(	$calendarURL == NULL || $calendarURL == "" )
	{
		$this->setData(['module', $this->getUrl(0), 'url', "module/calendar/placeholder_exemple.ics"]);
		$this->setData(['module', $this->getUrl(0), 'displayLangage', 'fr']);
		$this->setData(['module', $this->getUrl(0), 'startDate', '03/04/2023']);
		$this->setData(['module', $this->getUrl(0), 'endDate', '06/04/2023']);
		$this->setData(['module', $this->getUrl(0), 'timeZone', 'Europe/Paris']);
		$this->setData(['module', $this->getUrl(0), 'startHour', '08:00']);
		$this->setData(['module', $this->getUrl(0), 'endHour', '22:30']);
		$this->setData(['module', $this->getUrl(0), 'displayCalendar', true]);
		$this->setData(['module', $this->getUrl(0), 'displayDetails', true]);
		$this->setData(['module', $this->getUrl(0), 'displayURL', true]);
		$this->setData(['module', $this->getUrl(0), 'displayMobileCalendar', true]);
		$this->setData(['module', $this->getUrl(0), 'cellHeight', 50]);
		$this->setData(['module', $this->getUrl(0), 'eventDefaultBackgroundColor', 'rgba(225, 255, 255, 1)']);
		$this->setData(['module', $this->getUrl(0), 'eventDefaultTextColor', 'rgba(0, 0, 0, 1)']);
		$this->setData(['module', $this->getUrl(0), 'eventDefaultBordersColor', 'rgba(0, 0, 0, 1)']);
		$this->setData(['module', $this->getUrl(0), 'categories', array()]);
	}

	// Récupération des données du modules
	$timeZoneText = $this->getData(['module', $this->getUrl(0), 'timeZone']);
	$timeZone = new DateTimeZone($timeZoneText);

	$startDate = new DateTimeImmutable(str_replace("/","-",$this->getData(['module', $this->getUrl(0), 'startDate']))." ".$this->getData(['module', $this->getUrl(0), 'startHour']), $timeZone);

	$endDate = new DateTimeImmutable(str_replace("/","-",$this->getData(['module', $this->getUrl(0), 'endDate']))." ".$this->getData(['module', $this->getUrl(0), 'endHour']), $timeZone);
	$endDate = $endDate->add(new DateInterval("P1D"));

	$endHour = new DateTimeImmutable("2023-04-03 ".$this->getData(['module', $this->getUrl(0), 'endHour']), $timeZone);
	$numberOfDay = $startDate->diff($endDate)->format('%D');
	$numberOfHourPerDay = $startDate->diff($endHour)->format('%H');

	global $lang;
	$lang = $this->getData(['module', $this->getUrl(0), 'displayLangage']);
	$calendarURL = $this->getData(['module', $this->getUrl(0), 'url']);
	$cellHeight = $this->getData(['module', $this->getUrl(0), 'cellHeight']);
	$eventDefaultBackgroundColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultBackgroundColor']);
	$eventDefaultTextColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultTextColor']);
	$eventDefaultBordersColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultBordersColor']);

	$categories = $this->getData(['module', $this->getUrl(0), 'categories']);

	// Création de l'objet ICal à partir de l'URL fournie
	try {

		global $ical;

		$ical = new ICal('ICal.ics', array(
			'defaultSpan'                 => 2,     // Default value
			'defaultTimeZone'             => 'Europe/Paris',
			'defaultWeekStart'            => 'MO',  // Default value
			'disableCharacterReplacement' => false, // Default value
			'filterDaysAfter'             => null,  // Default value
			'filterDaysBefore'            => null,  // Default value
			'httpUserAgent'               => null,  // Default value
			'skipRecurrence'              => false, // Default value
		));
		if(filter_var($calendarURL, FILTER_VALIDATE_URL))
		{
			$ical->initUrl($calendarURL, $username = null, $password = null, $userAgent = null);
		}
		else {
			$ical->initFile($calendarURL);
		}

	} catch (\Exception $e) {
		die($e);
	}

	/**
	 * Comparaison de deux date ICal
	 * 
	 * @param object1 Première date sous format ICal
	 * @param object2 Seconde data sous format ICal
	 * 
	 * @return true si @date1 est supérieure à @date2
	 * @return false si @date1 est inférieure à @date2
	 * 
	 */
	function comparator($object1, $object2) {

		global $ical;

		return  $ical->iCalDateToDateTime($object1->dtstart_array[3])->format('H:i') > $ical->iCalDateToDateTime($object2->dtstart_array[3])->format('H:i');
	}

	$events = $ical->eventsFromRange($startDate->format('d-m-Y H:i'), $endDate->format('d-m-Y H:i'));
	usort($events, 'comparator');

	$intervalTotal = $startDate->diff($endDate);

	// DEBUG affichage du tableau d'events triés
	// foreach ($events as $key => $event) { echo "[".$key."] =>".$event->printData()."<br>";}

	// Traduction des jours et des mois
	function dayI18n($day){

		$days = array(
			"Monday" => "Lundi",
			"Tuesday" => "Mardi",
			"Wednesday" => "Mercredi",
			"Thursday" => "Jeudi",
			"Friday" => "Vendredi",
			"Saturday" => "Samedi",
			"Sunday" => "Dimanche",
		);
	
		$month = array(
			"January" => "Janvier",
			"February" => "Février",
			"March" => "Mars",
			"April" => "Avril",
			"May" => "Mai",
			"June" => "Juin",
			"July" => "Juillet",
			"August" => "Août",
			"September" => "Septembre",
			"October" => "Octobre",
			"November" => "Novembre",
			"December" => "Décembre",
		);

		global $lang;

		return ($lang == "fr" ? $days[$day->format('l')]." " : $day->format('l')." ").$day->format('j')." ".($lang == "fr" ? $month[$day->format('F')] : $day->format('F'));
	}
?>

<!-- Si affichage de la section de détail, on affiche les boutons pour défiler automatiquement jusqu'à un certain jour -->
<?php if ($this->getData(['module', $this->getUrl(0), 'displayDetails'])) {?>

	<section style="text-align:center;">
		<?php 
		$currentCalendarButtonDate = $startDate;
		
		for ($j=0; $j < $intervalTotal->format('%d'); $j++)
		{
			$dayTitle = dayI18n($currentCalendarButtonDate);

			echo "<a class='goToDay-button col2' style='background:".$eventDefaultBackgroundColor."; color:".$eventDefaultTextColor."; border-color:".$eventDefaultBordersColor."' href='javascript:void(0);' onclick='goToDay(\"#".str_replace(" ","",$dayTitle)."\")'>".$dayTitle."</a>";

			$currentCalendarButtonDate = $currentCalendarButtonDate->add(new DateInterval("P1D"));
		}
		?>
	</section> 

<?php }	?>

<!-- Si affichage de la section calendrier, on s'affiche. Vérification de l'affichage mobile -->
<?php if (($this->getData(['module', $this->getUrl(0), 'displayCalendar']) && !$detect->isMobile()) || ($this->getData(['module', $this->getUrl(0), 'displayMobileCalendar']) && $detect->isMobile())) {?>
<section aria-hidden="true" class="calendar-container">
	<div class="calendar">
		<div class="wrap">
			<table>
				<!-- Affichage de l'en-tête contenant chacun des jours -->
				<thead style="position: relative;">
					<tr> 
						<th class="headcol" style="visibility:hidden">Heure</th>
						<?php 
						$currentCalendarHeaderDate = $startDate;
						
						for ($j=0; $j < $intervalTotal->format('%d'); $j++)
						{
							echo "<th>".dayI18n($currentCalendarHeaderDate)."</th>";
							$currentCalendarHeaderDate = $currentCalendarHeaderDate->add(new DateInterval("P1D"));
						}
						?>
					</tr>
				</thead>

				<!-- Affichage du contenu du calendrier ligne par ligne -->
				<!-- Chaque colonne est un jour, chaque ligne est une tranche horaire (30min) -->
				<tbody>
					<?php

						$first = false;
						$currentCalendarDate = $startDate;

						for ($i=0; $i <= ($numberOfHourPerDay*2); $i++) {

							// Création de l'en-tête de ligne avec l'heure de la tranche horraire
							if($first){
								echo "</tr>";
								$first = false;
							}
							echo "<tr>";
							if ($currentCalendarDate->format('i') == "00")
							{
								echo "<td class='headcol' style='height:".$cellHeight."px'>".$currentCalendarDate->format('H:i')."</td>";
							}
							else {
								echo "<td class='headcol' style='visibility:hidden'>".$currentCalendarDate->format('H:i')."</td>";
							}

							// Création de chacune des cellule de la ligne
							for ($j=0; $j < $intervalTotal->format('%d'); $j++) {

								echo "<td style='height:".$cellHeight."px'>";

								///*DEBUG :*/echo $currentCalendarDate->format('d-m-Y H:i');

								// Pour chaque évènement
								foreach ($events as $event){

									$intervalStart = $currentCalendarDate->diff($ical->iCalDateToDateTime($event->dtstart_array[3]));

									///*DEBUG :*/echo $intervalStart->format('%a%H%I')."<br>";

									// Vérifier si l'évènement commence dans cet interval
									if (!$intervalStart->invert && (($intervalStart->days + $intervalStart->h) == 0) && $intervalStart->i < 30)
									{
										$intervalEnd = $currentCalendarDate->diff($ical->iCalDateToDateTime($event->dtend_array[3]));

										// hauteur d'une demi-heure - (pourcentage de la hauteur de la demi-heure commencé)
										$margin = (($intervalStart->i == 0) ? 0 : ((($intervalStart->i / 30 * 100) / 100) * $cellHeight));
									
										// ((Nombre d'heure * 2) + nombre de demi-heure)) * taille d'une demi-heure
										$height = ((($intervalEnd->h * 2)+($intervalEnd->i / 30)) * $cellHeight) - $margin;

										$eventBackgroundColor = $eventDefaultBackgroundColor;
										$eventTextColor = $eventDefaultTextColor;
										$eventBordersColor = $eventDefaultBordersColor;										

										// Choix de la couleur basée sur la catégorie
										if ($categories != "" && $event->categories && array_key_exists($event->categories, $categories))
										{
											$invalid_characters = array("$", "%", "#", "<", ">", "|", " ", "'", "\"", ",");
											$eventCatNameSafe = str_replace($invalid_characters,"",$event->categories);

											$eventBackgroundColor = $categories[$eventCatNameSafe]['eventBackgroundColor'];
											$eventTextColor = $categories[$eventCatNameSafe]['eventTextColor'];
											$eventBordersColor = $categories[$eventCatNameSafe][ 'eventBordersColor'];
										}
										
										// Construction de l'évènement
										echo "<a class='calendar-link event' style='background:".$eventBackgroundColor."; color:".$eventTextColor."; border-color:".$eventBordersColor."; height: ".str_replace(',','.',round($height, 1))."px; margin-top:". str_replace(',','.',round($margin, 1))."px' href='javascript:void(0);' onclick='openDetails(\"#id".$event->uid."\")'><strong>".$ical->iCalDateToDateTime($event->dtstart_array[3])->format('H:i')." ".$ical->iCalDateToDateTime($event->dtend_array[3])->format('H:i')."</strong></br>".$event->summary."</a>";		
										
										unset($event);
									}
								}

								echo "</td>";

								$currentCalendarDate = $currentCalendarDate->add(new DateInterval("P1D"));
							}

							$currentCalendarDate = $currentCalendarDate->sub(new DateInterval("P".$numberOfDay."D"));
							$currentCalendarDate = $currentCalendarDate->add(new DateInterval("PT30M"));
						}

						//echo count($events);
					?>
				</tbody>
			</table>
		</div>

		<!-- Si affichage du lien de téléchargement du calendrier, on l'affiche -->
		<?php if ($this->getData(['module', $this->getUrl(0), 'displayURL'])) {?>
			<div class="calendar-download" >
				<a href="<?php echo $calendarURL ?>" download="ihm23-calendar" target="_blank" >	<?php echo $lang == "fr" ? "Télécharger le calendrier iCal" : "Download iCal calendar";?></a>
			</div>
		<?php }	?>
	</div>
</section>
<?php }	?>

<!-- Si affichage de la section de détail, on l'affiche -->
<?php if ($this->getData(['module', $this->getUrl(0), 'displayDetails'])) {?>
<section>
	<?php 
	$currentDetailDate = $startDate;

	// Création d'une div pour chacun des jours
	for ($j=0; $j < $intervalTotal->format('%d'); $j++) {
		
		$endDay = $currentDetailDate->add(new DateInterval("P1D"));
		$events = $ical->eventsFromRange($currentDetailDate->format('d-m-Y H:i'), $endDay->format('d-m-Y H:i'));

		if (!empty($events)) {?>

			<div id="<?php echo $currentDetailDate->format('l') ?>" class="block">

			<?php 
				$dayTitle = dayI18n($currentDetailDate);
				
				echo '<h2 id="'.str_replace(" ","",$dayTitle).'" style="text-align: center;background-color:'.$eventDefaultBackgroundColor.'">'.$dayTitle.'</h2>';
			?>

			<!-- Création d'une liste pour contenir chacun des évènements de la journée -->
			<ul class="accordion" data-speed="150" title="<?php echo $dayTitle ?>">

				<?php
					foreach ($events as $event){

						$intervalStartD = ($currentDetailDate->format('d')) - ($ical->iCalDateToDateTime($event->dtstart_array[3]))->format('d');

						// Création d'un élément de liste pour chacun des évènements de la journée
						if ($intervalStartD == 0) {?>

							<li class="accordion-item">
								<h3 class="accordion-title" id="id<?php echo $event->uid; ?>">
									<span class="detail-title-TimeLoc">
										<?php echo $ical->iCalDateToDateTime($event->dtstart_array[3])->format('H:i')." - ".$ical->iCalDateToDateTime($event->dtend_array[3])->format('H:i'); if (!empty($event->location)) {echo" — ".$event->location;}?>
									</span>
									<strong>
										<?php echo $event->summary ?>
									</strong>

								</h3>
								<div class="accordion-content" style="display: none;">
									<?php echo $event->description ?>
								</div>
							</li>
			
						<?php }
					}?>
				</ul>
			</div>
		<?php }	?>
	<?php $currentDetailDate = $currentDetailDate->add(new DateInterval("P1D"));?>		
	<?php }	?>
</section>
<?php }	?>