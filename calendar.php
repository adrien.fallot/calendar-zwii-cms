<?php

/**
 * This file is part of the calendar module for Zwii.
 * For full copyright and license information, please see the LICENSE
 * file that was distributed with this source code.
 *
 * @author Adrien Fallot <https://gitlab.com/adrien.fallot>
 * @license GNU General Public License, version 3
 */

include 'vendor/ICal/ICal.php';
include 'vendor/ICal/Event.php';
include 'vendor/MobileDetect/MobileDetect.php';

use ICal\ICal;
use ICal\Event;
use MobileDetect\MobileDetect;

class calendar extends common {


	const VERSION = '2.0.0';
	const REALNAME = 'Calendar';
	const DELETE = true;
	const UPDATE = '0.0';
	const DATADIRECTORY = ''; // Contenu localisé inclus par défaut (page.json et module.json)
    
	public static $actions = [
		'config' => self::GROUP_MODERATOR,
		'index' => self::GROUP_VISITOR
	];
	
	public static $moduleLangage = [
		'fr' => 'Français',
		'en' => 'Anglais'
	];

	public static $categories = [];

	/**
	 * Configuration
	 */
	public function config() {

		$timeZoneText = $this->getData(['module', $this->getUrl(0), 'timeZone']);
		$timeZone = new DateTimeZone($timeZoneText);

		$calendarURL = $this->getData(['module', $this->getUrl(0), 'url']);

		$startDate = new DateTimeImmutable(str_replace("/","-",$this->getData(['module', $this->getUrl(0), 'startDate']))." ".$this->getData(['module', $this->getUrl(0), 'startHour']), $timeZone);

		$endDate = new DateTimeImmutable(str_replace("/","-",$this->getData(['module', $this->getUrl(0), 'endDate']))." ".$this->getData(['module', $this->getUrl(0), 'endHour']), $timeZone);
		$endDate = $endDate->add(new DateInterval("P1D"));
		
		// construction du calendrier 
		try {

			global $ical;
	
			$ical = new ICal('ICal.ics', array(
				'defaultSpan'                 => 2,     // Default value
				'defaultTimeZone'             => $timeZoneText,
				'defaultWeekStart'            => 'MO',  // Default value
				'disableCharacterReplacement' => false, // Default value
				'filterDaysAfter'             => null,  // Default value
				'filterDaysBefore'            => null,  // Default value
				'httpUserAgent'               => null,  // Default value
				'skipRecurrence'              => false, // Default value
			));
			// $ical->initFile('ICal.ics');
			$ical->initUrl($calendarURL, $username = null, $password = null, $userAgent = null);
	
		} catch (\Exception $e) {
			die($e);
		}

		$events = $ical->eventsFromRange($startDate->format('d-m-Y H:i'), $endDate->format('d-m-Y H:i'));

		$eventDefaultBackgroundColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultBackgroundColor']);
		$eventDefaultTextColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultTextColor']);
		$eventDefaultBordersColor = $this->getData(['module', $this->getUrl(0), 'eventDefaultBordersColor']);

		// Liste des catégories
		$categoriesData = $this->getData(['module', $this->getUrl(0), "categories"]);
		$invalid_characters = array("$", "%", "#", "<", ">", "|", " ", "'", "\"", ",");

		// Chercher des catégories parmi les évènements du calendrier
		foreach($events as $event)
		{
			$eventCatName = $event->categories;
			$eventCatNameSafe = str_replace($invalid_characters,"",$eventCatName);

			// Si elle n'existe pas déjà dans la configuration, on la rajoute
			if ( $categoriesData != "" && $eventCatNameSafe != "" && !array_key_exists($eventCatNameSafe, $categoriesData))
			{
				$categoriesData[$eventCatNameSafe] = [
					'name' => $eventCatName,
					'eventBackgroundColor' => $eventDefaultBackgroundColor,
					'eventTextColor' => $eventDefaultTextColor,
					'eventBordersColor' => $eventDefaultBordersColor
				];
			}
		}

		// S'il existe des catégories, les mettre en forme pour la configuration
		if($categoriesData) {

			//ksort($galleries);
			foreach($categoriesData as $categoryId => $category) {

				$categories[$categoryId] = [];

				// Met en forme le tableau meilleure solution $categories[count($categories)-1]
				self::$categories[] = [
					template::text('calendarConfigCategory'.$categoryId.'Name', [
						'readOnly' => true,
						'value' => $categoriesData[$categoryId]['name']
					]),
					template::text('calendarConfigCategory'.$categoryId.'BackgroundColor', [
						'class' => 'colorPicker',
						'value' => $categoriesData[$categoryId]['eventBackgroundColor']
					]),
					template::text('calendarConfigCategory'.$categoryId.'TextColor', [
						'class' => 'colorPicker',
						'value' => $categoriesData[$categoryId]['eventTextColor']
					]),
					template::text('calendarConfigCategory'.$categoryId.'BordersColor', [
						'class' => 'colorPicker',
						'value' => $categoriesData[$categoryId]['eventBordersColor']
					])
				];
				
			}
		}

		// Soumission du formulaire et sauvegarde des données de configuration
		if($this->isPost()) {
			$this->setData(['module', $this->getUrl(0), 'url', $this->getInput('calendarConfigUrl', helper::FILTER_URL, true)]);
			$this->setData(['module', $this->getUrl(0), 'displayLangage', $this->getInput('calendarConfigLang')]);
			$this->setData(['module', $this->getUrl(0), 'startDate', $this->getInput('calendarConfigStartDate')]);
			$this->setData(['module', $this->getUrl(0), 'endDate', $this->getInput('calendarConfigEndDate')]);
			$this->setData(['module', $this->getUrl(0), 'timeZone', $this->getInput('calendarConfigTimeZone')]);
			$this->setData(['module', $this->getUrl(0), 'startHour', $this->getInput('calendarConfigStartHour')]);
			$this->setData(['module', $this->getUrl(0), 'endHour', $this->getInput('calendarConfigEndHour')]);
			$this->setData(['module', $this->getUrl(0), 'displayCalendar', $this->getInput('calendarConfigDisplayCalendar', helper::FILTER_BOOLEAN)]);
			$this->setData(['module', $this->getUrl(0), 'displayDetails', $this->getInput('calendarConfigDisplayDetails', helper::FILTER_BOOLEAN)]);
			$this->setData(['module', $this->getUrl(0), 'displayURL', $this->getInput('calendarConfigDisplayURL', helper::FILTER_BOOLEAN)]);
			$this->setData(['module', $this->getUrl(0), 'displayMobileCalendar', $this->getInput('calendarConfigDisplayMobileCalendar', helper::FILTER_BOOLEAN)]);
			$this->setData(['module', $this->getUrl(0), 'cellHeight', $this->getInput('calendarConfigCellHeight', helper::FILTER_INT)]);
			$this->setData(['module', $this->getUrl(0), 'eventDefaultBackgroundColor', $this->getInput('calendarConfigDefaultBackgroundColor')]);
			$this->setData(['module', $this->getUrl(0), 'eventDefaultTextColor', $this->getInput('calendarConfigDefaultTextColor')]);
			$this->setData(['module', $this->getUrl(0), 'eventDefaultBordersColor', $this->getInput('calendarConfigDefaultBordersColor')]);

			$categories = [];

			foreach($categoriesData as $categoryId => $category)
			{
				$categories[$categoryId] = [
					'name' => $this->getInput('calendarConfigCategory'.$categoryId.'Name'),
					'eventBackgroundColor' => $this->getInput('calendarConfigCategory'.$categoryId.'BackgroundColor'),
					'eventTextColor' => $this->getInput('calendarConfigCategory'.$categoryId.'TextColor'),
					'eventBordersColor' => $this->getInput('calendarConfigCategory'.$categoryId.'BordersColor'),
				];
			}

			$this->setData(['module', $this->getUrl(0), 'categories', $categories]);

			// Valeurs en sortie
			$this->addOutput([
				'redirect' => helper::baseUrl() . $this->getUrl(),
				'notification' => 'Modifications enregistrées',
				'state' => true
			]);
		}
		// Valeurs en sortie
		$this->addOutput([
			'title' => 'Configuration du module',
			'vendor' => [
				'tinycolorpicker'
			],
			'view' => 'config'
		]);
	}

	/**
	 * Accueil
	 */
	public function index() {

		// Valeurs en sortie
		$this->addOutput([
			'showBarEditButton' => true,
			'showPageContent' => true,
			'view' => 'index'
		]);
	}
}
